var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/v1.0.0'))

app.listen(port);

console.log('Ejecutando Polymer desde Node on: ' + port);

app.get('/',function(req,res){
  res.sendFile("build/v1.0.0/index.html",{root: '.'});
});
app.get('/register-login',function(req,res){
  res.sendFile("build/v1.0.0/index.html",{root: '.'});
});
app.get('/agregar-viaje',function(req,res){
  res.sendFile("build/v1.0.0/index.html",{root: '.'});
});
app.get('/ver-movimientos',function(req,res){
  res.sendFile("build/v1.0.0/index.html",{root: '.'});
});
