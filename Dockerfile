# Imagen base
From node:latest

# Directorio de la app en el contenedor
WORKDIR /proy

# Copiado de archivos, con ADD . decimos que enviamos todos los archivos a ese directorio
ADD /build/v1.0.0 /proy/build/v1.0.0
ADD server.js /proy
ADD package.json /proy

# Dependencias
RUN npm install

# Puerto que se expone
EXPOSE 3000

#Comando con el que debe ejecutar docker nuestra aplicacion
CMD ["npm","start"]
